<?php
/* Smarty version 3.1.33, created on 2019-06-17 17:03:34
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d080026945560_09799425',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1556635595,
      2 => 'module',
    ),
    '3d5c6f19a897117cfc4c5de3e28c64167c451156' => 
    array (
      0 => '/home/dxintro/Sites/dev.latablitamachali.cod/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1556635595,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5d080026945560_09799425 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Productos Destacados
  </h2>
  <div class="products">
          
  <article class="product-miniature js-product-miniature" data-id-product="21" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://dev.latablitamachali.cod/index.php?id_product=21&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product" class="thumbnail product-thumbnail">
            <img
              src = "http://dev.latablitamachali.cod/img/p/2/4/24-home_default.jpg"
              alt = "cocktail n°1"
              data-full-size-image-url = "http://dev.latablitamachali.cod/img/p/2/4/24-large_default.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://dev.latablitamachali.cod/index.php?id_product=21&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product">cocktail n°1</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 CLP</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="22" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://dev.latablitamachali.cod/index.php?id_product=22&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product" class="thumbnail product-thumbnail">
            <img
              src = "http://dev.latablitamachali.cod/img/p/2/6/26-home_default.jpg"
              alt = "cocktail n°2"
              data-full-size-image-url = "http://dev.latablitamachali.cod/img/p/2/6/26-large_default.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://dev.latablitamachali.cod/index.php?id_product=22&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product">cocktail n°2</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 CLP</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="23" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://dev.latablitamachali.cod/index.php?id_product=23&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product" class="thumbnail product-thumbnail">
            <img
              src = "http://dev.latablitamachali.cod/img/p/2/8/28-home_default.jpg"
              alt = "cocktail n°3"
              data-full-size-image-url = "http://dev.latablitamachali.cod/img/p/2/8/28-large_default.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://dev.latablitamachali.cod/index.php?id_product=23&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product">cocktail n°3</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 CLP</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="24" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://dev.latablitamachali.cod/index.php?id_product=24&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product" class="thumbnail product-thumbnail">
            <img
              src = "http://dev.latablitamachali.cod/img/p/3/0/30-home_default.jpg"
              alt = "cocktail n°4"
              data-full-size-image-url = "http://dev.latablitamachali.cod/img/p/3/0/30-large_default.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://dev.latablitamachali.cod/index.php?id_product=24&amp;id_product_attribute=0&amp;rewrite=cocktail-n1&amp;controller=product">cocktail n°4</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 CLP</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>

    </div>
  </article>

      </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://dev.latablitamachali.cod/index.php?id_category=2&amp;controller=category">
    Todos los productos<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
